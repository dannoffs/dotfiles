set nocompatible
let mapleader = " " "set mapleader to spacebar
filetype plugin indent on

"basic ui settings
syntax enable
set number "show line numbers
filetype indent on "enables language-specific intentation
set wildmenu "shows autocomplete opitions in commands
set lazyredraw "only redraws when necessary, speeds up macros

"Custom mappings

"maps leader p to paste from system clipboard
nnoremap <leader>p "+gP

"whitespace
set tabstop=4
"set softtabstop=4
"set expandtab

"Set ColorScheme
let molokai_original = 1
colorscheme numix-molokai

"Gui-only configurations
set guioptions-=T "remove toolbar
set guioptions-=r "remove scrollbar
"end Gui-only configurations

set cursorline "Highlight current line
set showmatch "Highlight matching parens

"search settings
set incsearch "searches while typing
set hlsearch
nnoremap <leader><space> :nohlsearch<CR>

"code folding
set foldenable
set foldlevelstart=10 "opens most folds by default
set foldnestmax=10 "max number of folds
set foldmethod=indent "folds based on indentation


"Pathogen config
execute pathogen#infect()

""plugin specific configurations

"toggle gundo with leader + u
nnoremap <leader>u :GundoToggle<CR> 

" CtrlP settings
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'


